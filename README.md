# Chromatin loading

## 1. General description

Instructions for processing the data are written in the notebooks. Typically this involves reading a file stored in the `figure_data` folder and use custom functions to plot the data. The expected output is a localization histogram and stoichiometry distribution as presented in the manuscript.

## 2. Installation guide

Create a conda enviroment called chromatin-paper and activate it
```
conda create -n chromatin-paper python=3.10.11
conda activate chromatin-paper
```

Run the following command to install the required packages
```
pip install -r requirements.txt
```

If the required packages from `requirements.txt` are installed, the Jupyter Notebooks can be run in any IDE that supports them (like VS Code or the Jupyter Notebook web interface). Just make sure you are working in the right enviroment (chromatin-paper).




