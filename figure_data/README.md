# Data files description

The data used to plot figures is stored in this folder. A brief description of each file is as follows:

- dataLT: lifetime in seconds of single fluorophores associated to H2A histone.
- datab00ori**: H2A stoichiometry per fluorescent spot located at the origin.
- data00b**: localization in kbp from DNA center of each fluorescent spot.

where ** stands for internal experiment numbers. 


The `csv` files include all the data, meanwhile `yaml` files only include data used to generate the figures such as units, bar heights, values, etc. For further information, please refer to the manuscript